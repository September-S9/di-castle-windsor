﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CastleWindsor
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IWindsorContainer container = new WindsorContainer();
            container.Register(Component.For<IProgrammer>().ImplementedBy<CSharp>().Named("C#"));
            container.Register(Component.For<IProgrammer>().ImplementedBy<Java>().Named("java"));
            container.Register(Component.For<IProgrammer>().ImplementedBy<VB>().Named("vb"));
            var programer = container.Resolve<IProgrammer>("C#");
            programer.Wtite();

            var programerJava = container.Resolve<IProgrammer>("java");
            programerJava.Wtite();

            var programerVB = container.Resolve<IProgrammer>("vb");
            programerVB.Wtite();

            Console.ReadLine();
        }
    }
}
